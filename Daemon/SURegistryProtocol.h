//
//  SURegistryProtocol.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SUProduct;
@protocol SURegistryProtocol
- (BOOL)registerProduct:(in bycopy SUProduct *)product withIdentifier:(in bycopy NSString *)identifier;
@end
