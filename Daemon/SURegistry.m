//
//  SURegistry.m
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SURegistry.h"
#import "SUProduct.h"

@interface SURegistry (Private)
+ (NSString *)registryPath;
- (BOOL)saveRegistry;
@end

@implementation SURegistry

+ (SURegistry *)sharedRegistry
{
	static SURegistry *sharedRegistry = nil;
	if (!sharedRegistry)
		sharedRegistry = [[SURegistry alloc] init];
	return sharedRegistry;
}

- (BOOL)registerProduct:(SUProduct *)product withIdentifier:(NSString *)identifier;
{
	[products setObject:product forKey:identifier];
	[self saveRegistry];
	return YES;
}

- (BOOL)saveRegistry
{	
	// Make sure we can write to the registry.
	if ([[NSFileManager defaultManager] fileExistsAtPath:[[self class] registryPath]] && ![[NSFileManager defaultManager] isWritableFileAtPath:[[self class] registryPath]])
		[NSException raise:@"SURegistryProtected" format:@"Don't have permission to write to the registry (%@)!", [[self class] registryPath]];

	if (![[NSFileManager defaultManager] isWritableFileAtPath:SUSupportFolderPath()])
	{
		if (!SUSetupSupportFolder())
			return NO; // Unlike the above error, this we don't throw an exception for because the user knowingly didn't authenticate.
	}

	return [NSKeyedArchiver archiveRootObject:self toFile:[[self class] registryPath]];
}

+ (NSString *)registryPath
{
	return [SUSupportFolderPath() stringByAppendingPathComponent:@"Registry.plist"];
}

- init
{
	// First, try loading from file; if it doesn't exist, we'll make a new one.
	SURegistry *newRegistry = [NSKeyedUnarchiver unarchiveObjectWithFile:[[self class] registryPath]];
	if (newRegistry) { return [newRegistry retain]; }
	
	[super init];
	products = [[NSMutableDictionary dictionary] retain];
	return self;
}

- (void)dealloc
{
	[products release];
	[super dealloc];
}

- initWithCoder:(NSCoder *)coder
{
	[super init];
	products = [[coder decodeObjectForKey:@"products"] retain];
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:products forKey:@"products"];
}

+ (void)initilize
{
	// We encode the version of the registry being saved so that if the format changes in the future, we can handle that.
	[self setVersion:[[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] intValue]];
}

@end
