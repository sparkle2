//
//  SURegistry.h
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SURegistryProtocol.h"

@interface SURegistry : NSObject <SURegistryProtocol, NSCoding> {
	NSMutableDictionary *products;
}

+ (SURegistry *)sharedRegistry;
- (BOOL)registerProduct:(SUProduct *)product withIdentifier:(NSString *)identifier;

@end
