//
//  main.m
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright (c) 2007 Andy Matuschak. All rights reserved.
//

#import "SURegistry.h"

int main(int argc, const char *argv[])
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSObject *rootObject = [SURegistry sharedRegistry];
	
	NSConnection *conn = [[NSConnection defaultConnection] retain];
	[conn setRootObject:rootObject];
	[conn registerName:SUDaemonServiceName];
	
	[[NSRunLoop currentRunLoop] run];
	
	[pool release];
	return 0;
}