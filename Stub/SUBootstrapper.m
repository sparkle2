//
//  SUBootstrapper.m
//  Sparkle
//
//  Created by Andy Matuschak on 9/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUBootstrapper.h"
#import "SURegistrar.h"

@implementation SUBootstrapper

- (void)awakeFromNib
{
	// For easy installation, we'll register the app when this is loaded from the nib.
	[[SURegistrar sharedRegistrar] attemptRegistration];
}

@end
