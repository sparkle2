//
//  SURegistrar.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SUProduct;
@interface SURegistrar : NSObject {
	id delegate;
}

+ (SURegistrar *)sharedRegistrar;
- (void)attemptRegistration;
- (void)setDelegate:(id)delegate;
@end

@interface NSObject (SUBootstrapperDelegateProtocol)
- (SUProduct *)productForRegistrar:(SURegistrar *)registrar;
- (NSString *)productIdentifierForRegistrar:(SURegistrar *)registrar;
@end
