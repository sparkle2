//
//  SURegistrar.m
//  Sparkle
//
//  Created by Andy Matuschak on 9/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SURegistrar.h"
#import "SURegistryProtocol.h"
#import "SUProduct.h"
#import "BLAuthentication.h"

NSString *SULaunchCountKey = @"SULaunchCount";
NSString *SUHasPromptedUserKey = @"SUHasPromptedUser";
NSString *SULaunchesRequiredForPromptKey = @"SULaunchesRequiredForPrompt";

@interface SURegistrar (Private)
- (int)launchCount;
- (int)launchesRequiredForPrompt;
- (BOOL)isNewSparkleUser;
- (void)recordPrompt;
- (void)attemptInstallation;
- (void)updateAppRegistration:(id <SURegistryProtocol>)registry;
- (NSString *)productIdentifier;
@end

@implementation SURegistrar

+ (SURegistrar *)sharedRegistrar
{
	static SURegistrar *sharedRegistrar = nil;
	if (sharedRegistrar == nil)
		sharedRegistrar = [[SURegistrar alloc] init];
	return sharedRegistrar;
}

// This method attempts to inform the Sparkle daemon about this app. If the Sparkle daemon isn't installed,
// it will ask the user if he wants to install it, but only if he's never been asked before and the app has been launched enough times.
- (void)attemptRegistration
{
	// First, we try to connect to the Sparkle daemon.
	id proxy = [[NSConnection rootProxyForConnectionWithRegisteredName:SUDaemonServiceName host:nil] retain];
	if (proxy)
	{
		[proxy setProtocolForProxy:@protocol(SURegistryProtocol)];
		// We update the application's registration every time it launches in case something changes.
		[self updateAppRegistration:proxy];
	}
	else
	{
		// Either the daemon isn't running right now, or the user is new to Sparkle.
		if ([self isNewSparkleUser])
		{
			// They're a Sparkle virgin! But we don't want to disrupt the first run of this app, so have we launched enough times?
			if ([self launchCount] >= [self launchesRequiredForPrompt])
				[self attemptInstallation];
		}
		// Otherwise, the daemon isn't running, or they just don't want Sparkle. So we're done.
	}
}

- (void)updateAppRegistration:(id <SURegistryProtocol>)registry
{
	SUProduct *product;
	if ([delegate respondsToSelector:@selector(productForRegistrar:)]) // Does the delegate want to take control?
	{
		product = [delegate productForRegistrar:self];
	}
	else
	{
		// The app doesn't want any special behavior, so it's probably just a plain .app.
		product = [[[SUProduct alloc] initWithMainBundle:[NSBundle mainBundle]] autorelease];
	}
	[registry registerProduct:product withIdentifier:[self productIdentifier]];
}

- (void)attemptInstallation
{
	// First, though, we have to ask.
	[self recordPrompt];
	NSAlert *alert = [NSAlert alertWithMessageText:@"Would you like to keep your applications up-to-date?" defaultButton:@"Install Sparkle" alternateButton:@"Don't Install" otherButton:@"Learn More..." informativeTextWithFormat:@"Sparkle keeps APP_NAME and other supported applications up-to-date. Would you like to install Sparkle now?"];
	[alert setIcon:[[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForImageResource:@"Sparkle"]]];
	[alert runModal];
	
	SUSetupSupportFolder();
}

- (void)recordPrompt
{
	// Right now, the behavior is that we're asking every user individually if he wants to install Sparkle so as to avoid authenticating for /Library.
	// This is probably sub-optimal.
	CFPreferencesSetValue((CFStringRef)SUHasPromptedUserKey, [NSNumber numberWithBool:YES], (CFStringRef)SUSparkleIdentifier, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	CFPreferencesSynchronize((CFStringRef)SUSparkleIdentifier, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
}

- (NSString *)productIdentifier
{
	if ([delegate respondsToSelector:@selector(productIdentifierForRegistrar:)]) // Give the delegate a chance to provide a custom identifier.
		return [delegate productIdentifierForRegistrar:self];
	return [[NSBundle mainBundle] bundleIdentifier];
}

- (BOOL)isNewSparkleUser
{
	BOOL hasPromptedUser;
	Boolean validKey;
	hasPromptedUser = CFPreferencesGetAppBooleanValue((CFStringRef)SUHasPromptedUserKey, (CFStringRef)SUSparkleIdentifier, &validKey);
	return !(hasPromptedUser && validKey);
}

- (void)setDelegate:(id)aDelegate
{
	delegate = aDelegate;
}

+ (void)initialize
{
	// Whenever the registrar is initialized, we increment the launch count. This assumes that the class is loaded on every launch, which may not be the case.
	[[NSUserDefaults standardUserDefaults] setInteger:([[NSUserDefaults standardUserDefaults] integerForKey:SULaunchCountKey] + 1) forKey:SULaunchCountKey];
}

- (int)launchCount
{
	return [[NSUserDefaults standardUserDefaults] integerForKey:SULaunchCountKey];
}

- (int)launchesRequiredForPrompt
{
	NSNumber *requirement = [[NSBundle mainBundle] objectForInfoDictionaryKey:SULaunchesRequiredForPromptKey]; 
	if (!requirement) { return 2; } // Default is to prompt on the *second* launch.
	return [requirement intValue];
}

@end