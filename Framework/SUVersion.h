//
//  SUVersion.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/13/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SUDownload;
@interface SUVersion : NSObject {
	NSString *bundleVersion;
	NSString *humanVersion;
	NSString *requiredSystemVersion;
	
	NSDate *releaseDate;
	id releaseNotes;
	
	SUDownload *download;
	NSDictionary *diffs;
}

- (NSString *)bundleVersion;
- (NSString *)humanVersion;
- (NSArray *)supersededVersions;
//- (SUBranch *)branch;

- (NSDate *)releaseDate;

- releaseNotes;

- (NSString *)requiredSystemVersion;

@end
