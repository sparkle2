//
//  SUPoset.h
//  Sparkle
//
//  Created by David Symonds on 17/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


typedef enum {
	SUPosetOrderAscending = -1,
	SUPosetOrderUnknown = 0,
	SUPosetOrderDescending = 1,
} SUPosetOrder;

@interface SUPoset : NSObject {
	NSMutableArray *groundSet;
	NSMutableArray *declaredOrderings;
	NSMutableDictionary *branchLabels;	// maps objects in groundSet to NSString branch labels
}

- (SUPoset *)init;

- (void)clearPoset;

- (void)setOrdering:(id)object1 toObject:(id)object2 as:(SUPosetOrder)order;
- (void)setOrdering:(id)object1 comesBefore:(id)object2;
- (void)setBranchLabel:(NSString *)label onObject:(id)object;

- (SUPosetOrder)compare:(id)object1 toObject:(id)object2;
- (NSString *)branchLabelForObject:(id)object;

@end
