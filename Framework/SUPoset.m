//
//  SUPoset.m
//  Sparkle
//
//  Created by David Symonds on 17/09/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUPoset.h"


@interface SUPoset (Private)

- (NSArray *)lesserElementsThan:(id)object;
- (NSArray *)greaterElementsThan:(id)object;
- (NSMutableArray *)bfsFromObject:(id)object usingOrder:(SUPosetOrder)order stopAtLabels:(BOOL)labelStop;

@end

#pragma mark -

int cmpByPoset(id object1, id object2, void *context)
{
	SUPoset *poset = (SUPoset *) context;
	SUPosetOrder order = [poset compare:object1 toObject:object2];

	if (order == SUPosetOrderAscending)
		return NSOrderedAscending;
	else if (order == SUPosetOrderDescending)
		return NSOrderedDescending;
	else
		return NSOrderedSame;		// XXX: this should never be returned!
}

@implementation SUPoset

- (SUPoset *)init
{
	if (!(self = [super init]))
		return nil;

	groundSet = [[NSMutableArray alloc] init];
	declaredOrderings = [[NSMutableArray alloc] init];
	branchLabels = [[NSMutableDictionary alloc] init];

	return self;
}

- (void)dealloc
{
	[groundSet release];
	[declaredOrderings release];
	[branchLabels release];

	[super dealloc];
}

#pragma mark -

- (void)clearPoset
{
	[groundSet removeAllObjects];
	[declaredOrderings removeAllObjects];
	[branchLabels removeAllObjects];
}

#pragma mark -

- (void)setOrdering:(id)object1 toObject:(id)object2 as:(SUPosetOrder)order
{
	if (order == SUPosetOrderUnknown)
		[NSException raise:@"Logic Error" format:@"Must specify a proper ordering for Poset elements"];
	if ([object1 isEqual:object2])
		[NSException raise:@"Logic Error" format:@"Can't specify an ordering between identical elements"];

	if (![groundSet containsObject:object1])
		[groundSet addObject:object1];
	if (![groundSet containsObject:object2])
		[groundSet addObject:object2];

	if (order == SUPosetOrderAscending)
		[declaredOrderings addObject:[NSArray arrayWithObjects:object1, object2, nil]];
	else
		[declaredOrderings addObject:[NSArray arrayWithObjects:object2, object1, nil]];
}

- (void)setOrdering:(id)object1 comesBefore:(id)object2
{
	[self setOrdering:object1 toObject:object2 as:SUPosetOrderAscending];
}

- (void)setBranchLabel:(NSString *)label onObject:(id)object
{
	if (![groundSet containsObject:object])
		[groundSet addObject:object];
	[branchLabels setValue:label forKey:object];
}

- (SUPosetOrder)compare:(id)object1 toObject:(id)object2
{
	if (![groundSet containsObject:object1] || ![groundSet containsObject:object2])
		return SUPosetOrderUnknown;

	// Ugh! TODO: Make this more efficient!
	if ([[self greaterElementsThan:object1] containsObject:object2])
		return SUPosetOrderAscending;
	if ([[self lesserElementsThan:object1] containsObject:object2])
		return SUPosetOrderDescending;

	return SUPosetOrderUnknown;
}

- (NSString *)branchLabelForObject:(id)object
{
	if (![groundSet containsObject:object])
		return nil;

	// Handle easy case first: this object is directly labelled
	NSString *label;
	if ((label = [branchLabels objectForKey:object]))
		return label;

	// Find all objects that are "less" than this object, and ignore those objects that aren't labelled
	NSMutableArray *bfs = [self bfsFromObject:object usingOrder:SUPosetOrderDescending stopAtLabels:YES];
	NSMutableSet *labels = [NSMutableSet setWithArray:bfs];
	[labels intersectSet:[NSSet setWithArray:[branchLabels allKeys]]];
	[bfs setArray:[labels allObjects]];

	if ([bfs count] == 0)
		return nil;

	[bfs sortUsingFunction:cmpByPoset context:self];
	return [branchLabels objectForKey:[bfs objectAtIndex:0]];
}

#pragma mark -
#pragma mark Private helper methods

// Return all the elements that are comparable to the given object, and are "less" than it
- (NSArray *)lesserElementsThan:(id)object
{
	NSMutableArray *array = [self bfsFromObject:object usingOrder:SUPosetOrderDescending stopAtLabels:NO];
	[array removeObjectAtIndex:0];
	return array;
}

// Return all the elements that are comparable to the given object, and are "less" than it
- (NSArray *)greaterElementsThan:(id)object
{
	NSMutableArray *array = [self bfsFromObject:object usingOrder:SUPosetOrderAscending stopAtLabels:NO];
	[array removeObjectAtIndex:0];
	return array;
}

- (NSMutableArray *)bfsFromObject:(id)object usingOrder:(SUPosetOrder)order stopAtLabels:(BOOL)labelStop
{
	NSMutableArray *array = [NSMutableArray arrayWithObject:object];
	int index = 0;

	// TODO: Optimise this, if needed
	while (index < [array count]) {
		object = [array objectAtIndex:index];
		++index;

		if (labelStop && [branchLabels objectForKey:object])
			continue;

		NSEnumerator *en = [declaredOrderings objectEnumerator];
		NSArray *ordering;
		while ((ordering = [en nextObject])) {
			id neighbour = nil;
			if ((order == SUPosetOrderAscending) && ([[ordering objectAtIndex:0] isEqual:object]))
				neighbour = [ordering objectAtIndex:1];
			else if ((order == SUPosetOrderDescending) && ([[ordering objectAtIndex:1] isEqual:object]))
				neighbour = [ordering objectAtIndex:0];
			if (neighbour && ![array containsObject:neighbour])
				[array addObject:neighbour];
		}
	}

	return array;
}

@end
