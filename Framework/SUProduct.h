//
//  SUProduct.h
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class NDAlias;
@interface SUProduct : NSObject <NSCoding, NSCopying> {
	NSString *name;
	NSImage *icon;
	NDAlias *iconAlias;
	NSURL *feedURL;
	
	NDAlias *mainBundleAlias;
	NSMutableArray *pathAliases;
}

- (SUProduct *)initWithMainBundle:(NSBundle *)mainBundle;

// We're abstracting away the NDAlias ugliness here.
- (NSString *)iconPath;
- (void)setIconPath:(NSString *)iconPath;

// KVC compliance for paths, abstracting away NDAlias ugliness.
- (NSArray *)paths;
- (void)insertObject:(NSString *)path inPathsAtIndex:(int)index;
- (void)removeObjectFromPathsAtIndex:(int)index;
- (void)removeAllObjectsFromPaths;

@end
