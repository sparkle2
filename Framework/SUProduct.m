//
//  SUProduct.m
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUProduct.h"
#import "NDAlias/NDAlias.h"

@interface SUProduct (Private)
- (NSImage *)genericApplicationIcon;
- (NSArray *)codedKeys;
@end

@implementation SUProduct

- (SUProduct *)initWithMainBundle:(NSBundle *)bundle
{
	[super init];
	// Create an alias to the icon URL.
	NSString *iconPath = [bundle pathForImageResource:@"NSApplicationIcon"];
	if (iconPath != nil)
		[self setIconPath:[bundle pathForImageResource:@"NSApplicationIcon"]];
	
	// Find the product name from the bundle.
	NSString *productName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
	if (!productName)
		productName = [[[NSFileManager defaultManager] displayNameAtPath:[bundle bundlePath]] stringByDeletingPathExtension]; // Make one up from the path.
	[self setValue:productName forKey:@"name"];
	
	// Find the Sparkle feed URL.
	NSString *feed = [bundle objectForInfoDictionaryKey:@"SUFeedURL"];
	if (!feed)
		[NSException raise:@"SUNoFeedException" format:@"You must include an SUFeedURL key in Info.plist for Sparkle to know where to look!"];
	[self setValue:[NSURL URLWithString:feed] forKey:@"feedURL"];
	
	// Now we set the bundle as the first path in the list of paths.
	[self insertObject:[bundle bundlePath] inPathsAtIndex:0];
	mainBundleAlias = [pathAliases objectAtIndex:0]; // Keep a pointer to the main bundle so we can get the version from it.
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	if ([coder allowsKeyedCoding])
	{
		NSEnumerator *keyEnumerator = [[self codedKeys] objectEnumerator];
		NSString *key;
		while (key = [keyEnumerator nextObject])
		{
			[coder encodeObject:[self valueForKey:key] forKey:key];
		}
		[coder encodeConditionalObject:mainBundleAlias forKey:@"mainBundleAlias"]; // Encode this separately, since it's just a reference.
	}
	else
	{
		// NSPortCoder doesn't support keyed coding, so we just send over the archived data.
		[coder encodeDataObject:[NSKeyedArchiver archivedDataWithRootObject:self]];
	}
}

- initWithCoder:(NSCoder *)coder
{
	// NSPortCoder doesn't support keyed coding, so we just send over the archived data.
	if ([coder allowsKeyedCoding])
	{
		[super init];
		NSEnumerator *keyEnumerator = [[self codedKeys] objectEnumerator];
		NSString *key;
		while (key = [keyEnumerator nextObject])
		{
			[self setValue:[coder decodeObjectForKey:key] forKey:key];
		}
		mainBundleAlias = [coder decodeObjectForKey:@"mainBundleAlias"]; // Decode this separately, since it's just a reference.
		return self;
	}
	else
	{
		// NSPortCoder doesn't support keyed coding, so we just take archived data and make a new product from that.
		return [NSKeyedUnarchiver unarchiveObjectWithData:[coder decodeDataObject]];
	}
}

- copyWithZone:(NSZone *)zone
{
	return [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self]];
}

// Keys that should be archived for NSCoding.
- (NSArray *)codedKeys
{
	return [NSArray arrayWithObjects:@"name", @"iconAlias", @"feedURL", @"pathAliases", nil];
}

- (NSImage *)icon
{
	if (!icon) // Lazily cache the icon.
	{
		if ([self valueForKey:@"iconAlias"])
			icon = [[NSImage alloc] initWithContentsOfFile:[[self valueForKey:@"iconAlias"] path]];
		else
			return [self genericApplicationIcon]; // In case the app doesn't have an icon.
	}
	return icon;
}

// Abstract away the NDAlias for the icon path.
- (NSString *)iconPath
{
	return [[self valueForKey:@"iconAlias"] path];
}

- (void)setIconPath:(NSString *)path
{
	[icon release];
	icon = nil; // We'll want to reload it next time it's asked for.
	[self setValue:[NDAlias aliasWithPath:path] forKey:@"iconAlias"];
}

// Put a nice KVC-compliant wrapper around the paths to get around the NDAlias messiness.
- (NSArray *)paths
{
	return [pathAliases valueForKey:@"path"];
}

- (void)insertObject:(NSString *)path inPathsAtIndex:(int)index
{
	if (pathAliases == nil) { pathAliases = [[NSMutableArray array] retain]; }
	if (![[pathAliases valueForKey:@"path"] containsObject:path])
		[pathAliases insertObject:[NDAlias aliasWithPath:path] atIndex:index];
}

- (void)removeObjectFromPathsAtIndex:(int)index
{
	[pathAliases removeObjectAtIndex:index];
}

- (void)removeAllObjectsFromPaths
{
	[pathAliases removeAllObjects];
}

- (void)dealloc
{
	[name release];
	[icon release];
	[iconAlias release];
	[feedURL release];
	[pathAliases release];
	[super dealloc];
}

- (NSImage *)genericApplicationIcon
{
	return [[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericApplicationIcon)];
}

// We implement this to let the coder know that we actually want to send the *contents* over the line when it's passed bycopy.
- replacementObjectForPortCoder:(NSPortCoder *)encoder
{
    if ([encoder isByref])
		return [NSDistantObject proxyWithLocal:self connection:[encoder connection]];
    else
		return self;
}

@end
