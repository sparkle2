//
//  SUBranch.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/13/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SUBranch : NSObject {
	NSArray *labels;
}

//- (BOOL)isRelevantToLabel:(SUBranchLabel *)label;
- (BOOL)isPaid;

@end
