//
//  SUDownload.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/17/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SUDownload : NSObject {
	NSURL *URL;
	NSString *signature;
}

- initWithURL:(NSURL *)URL DSASignature:(NSString *)signature;

@end
