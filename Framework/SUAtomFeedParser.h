//
//  SUAtomFeedParser.h
//  Sparkle
//
//  Created by Andy Matuschak on 9/13/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class XMLTree, SUPoset;
@interface SUAtomFeedParser : NSObject {
	XMLTree *document;
	
	NSMutableDictionary *versions; // A version string -> SUVersion mapping to help sort things into the poset.
	SUPoset *poset;
}

- (SUPoset *)parseData:(NSData *)data;

@end
