//
//  SUAtomFeedParser.m
//  Sparkle
//
//  Created by Andy Matuschak on 9/13/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUAtomFeedParser.h"
#import "SUVersion.h"
#import "SUDownload.h"
#import "XMLTree.h"
#import "SUPoset.h"

NSString *SUAtomNamespaceURI = @"http://sparkle.andymatuschak.org/2.0";

@interface SUAtomFeedParser (Private)
- (NSString *)prefix;
- (void)parseVersionWithEntry:(XMLTree *)entry;
- (void)orderVersionWithEntry:(XMLTree *)entry;
- (SUDownload *)parseDownload:(XMLTree *)downloadNode;
@end

@implementation SUAtomFeedParser

- (SUPoset *)parseData:(NSData *)data
{
	versions = [NSMutableDictionary dictionary];
	poset = [[[SUPoset alloc] init] autorelease];
	
	document = [[XMLTree alloc] initWithData:data withResolvingURL:nil];
	if (!document)
		[NSException raise:@"SUAtomFeedParseError" format:@"Couldn't parse the update feed. Try using an XML validation tool on it to find the problem."];
	
	// First, we parse all the information into SUVersions.
	NSArray *entries = [[document childNamed:@"feed"] childrenNamed:@"entry"];
	id entriesEnumerator = [entries objectEnumerator], currentEntry;
	while (currentEntry = [entriesEnumerator nextObject])
		[self parseVersionWithEntry:currentEntry];

	// Now that we have filled out all the versions, we build up the poset using the "supersedesVersion" information.
	entriesEnumerator = [entries objectEnumerator];
	while (currentEntry = [entriesEnumerator nextObject])
		[self orderVersionWithEntry:currentEntry];
	return poset;
}

- (void)parseVersionWithEntry:(XMLTree *)entry
{
	SUVersion *version = [[[SUVersion alloc] init] autorelease];
	[version setValue:[NSDate dateWithNaturalLanguageString:[[entry childNamed:@"updated"] description]] forKey:@"releaseDate"];
	
	// Parse release notes, which can either be a link or content embedded inline.
	id content = [entry childNamed:@"content"];
	if ([content attributeNamed:@"src"])
		[version setValue:[NSURL URLWithString:[content attributeNamed:@"src"]] forKey:@"releaseNotes"];
	else
		[version setValue:[content description] forKey:@"releaseNotes"];
	
	XMLTree *releaseInfo = [entry childWithLocalName:@"release" prefix:[self prefix]];
	[version setValue:[[releaseInfo childWithLocalName:@"humanVersion" prefix:[self prefix]] description] forKey:@"humanVersion"];
	[version setValue:[[releaseInfo childWithLocalName:@"requires" prefix:[self prefix]] attributeNamed:@"systemVersion"] forKey:@"requiredSystemVersion"];
		
	// Parse the bundle version; this is required, so enforce its presence.
	NSString *bundleVersion = [releaseInfo attributeNamed:@"version"];
	if (bundleVersion == nil)
		[NSException raise:@"SUAtomMissingVersionInfo" format:@"One of your sparkle:release tags has no version attribute."];
	[version setValue:bundleVersion forKey:@"bundleVersion"];
	
	// Every release must have a full download link in addition to potential diffs in case the integrity of the installation is compromised.
	SUDownload *download = [self parseDownload:[releaseInfo childWithLocalName:@"download" prefix:[self prefix]]];
	if (download == nil)
		[NSException raise:@"SUAtomMissingDownloadInfo" format:@"Your <sparkle:release> element for version %@ has missing or incomplete download information.", bundleVersion];
	[version setValue:download forKey:@"download"];
	
	// Parse the diffs; there can be many since they can be for upgrades from different versions.
	NSArray *diffNodes = [releaseInfo childrenWithLocalName:@"diff" prefix:[self prefix]];
	id enumerator = [diffNodes objectEnumerator], current;
	NSMutableDictionary *diffs = [NSMutableDictionary dictionary];
	while (current = [enumerator nextObject])
	{
		[diffs setObject:[self parseDownload:current] forKey:[[current childWithLocalName:@"fromVersion" prefix:[self prefix]] description]];
	}
	[version setValue:diffs forKey:@"diffs"];
	
	// Now store this version in the orderings dictionary, with the identifiers of the versions it supersedes as the value.
	if ([versions objectForKey:bundleVersion] != nil) // No duplicates allowed!
		[NSException raise:@"SUAtomDuplicateVersion" format:@"You have two <sparkle:release> tags with version=%@!", bundleVersion];
	[versions setObject:version forKey:bundleVersion];
}

- (void)orderVersionWithEntry:(XMLTree *)entry
{
	XMLTree *releaseInfo = [entry childWithLocalName:@"release" prefix:[self prefix]];
	NSString *versionKey = [releaseInfo attributeNamed:@"version"];
	
	id supersededVersionEnumerator = [[[releaseInfo childrenWithLocalName:@"supersedesVersion" prefix:[self prefix]] valueForKey:@"description"] objectEnumerator], supersededVersionKey;
	while (supersededVersionKey = [supersededVersionEnumerator nextObject])
	{
		SUVersion *supersededVersion = [versions objectForKey:supersededVersionKey];
		if (supersededVersion == nil)
			[NSException raise:@"SUAtomMissingVersion" format:@"Version %@ is listed as superseding version %@, but that version doesn't exist!", versionKey, supersededVersionKey];
		[poset setOrdering:supersededVersion comesBefore:[versions objectForKey:versionKey]];
	}
}

- (SUDownload *)parseDownload:(XMLTree *)downloadNode
{
	NSURL *URL = [NSURL URLWithString:[downloadNode attributeNamed:@"src"]];
	NSString *DSASignature = [[downloadNode childWithLocalName:@"dsaSignature" prefix:[self prefix]] description];
	return [[[SUDownload alloc] initWithURL:URL DSASignature:DSASignature] autorelease];
}


// This function returns the XML prefix being used in the feed for the Sparkle namespace.
// e.g: for <sparkle:release>, the prefix is sparkle.
- (NSString *)prefix
{
	// We'll do this by checking out the namespace attributes on the feed element.
	NSDictionary *attributes = [[document childNamed:@"feed"] attributes];
	id enumerator = [[attributes allKeys] objectEnumerator], current;
	while (current = [enumerator nextObject])
	{
		// Find the one with Sparkle's namespace URI; that'll be the correct prefix.
		if ([[attributes objectForKey:current] isEqualToString:SUAtomNamespaceURI])
		{
			@try
		{
			return [[current componentsSeparatedByString:@"xmlns:"] objectAtIndex:1]; // Get rid of the xmlns: prefix.
		}
			@catch (NSException *e) {
				[NSException raise:@"SUAtomFeedNamespaceError" format:@"Found the Sparkle namespace entry, but couldn't figure out the prefix. The attribute on <feed> should have a name starting with xmlns:. Yours is named: %@.", current];
			}
		}
	}
	[NSException raise:@"SUAtomFeedNamespaceError" format:@"This update feed doesn't include the Sparkle namespace, which is necessary for meta information about versions and so on. Make sure your <feed> element has a namespace pointing to %@.", SUAtomNamespaceURI];
	return nil;
}

@end
