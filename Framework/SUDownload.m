//
//  SUDownload.m
//  Sparkle
//
//  Created by Andy Matuschak on 9/17/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUDownload.h"


@implementation SUDownload

- initWithURL:(NSURL *)aURL DSASignature:(NSString *)aSignature
{
	[super init];
	[self setValue:aURL forKey:@"URL"];
	[self setValue:aSignature forKey:@"signature"];
	return self;
}

@end
