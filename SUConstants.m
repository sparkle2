//
//  SUConstants.m
//  Sparkle
//
//  Created by Andy Matuschak on 8/10/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

NSString *SUDaemonServiceName = @"org.andymatuschak.sparkledaemon";
NSString *SUSparkleIdentifier = @"org.andymatuschak.sparkle";
NSString *SUProductRegistryKey = @"SUProductRegistry";