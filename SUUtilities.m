//
//  SUUtilities.m
//  Sparkle
//
//  Created by Andy Matuschak on 9/12/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SUUtilities.h"
#import "BLAuthentication.h"

// Make sure it's really /Library/Application Support/Sparkle
NSString *SUSupportFolderPath()
{
    FSRef folder;
    OSErr err = noErr;
    CFURLRef url;
    NSString *result = nil;
	
    err = FSFindFolder(kOnSystemDisk, kApplicationSupportFolderType, false, &folder);
    if (err == noErr) {
        url = CFURLCreateFromFSRef(kCFAllocatorDefault, &folder);
        result = [[(NSURL *)url path] stringByAppendingPathComponent:@"Sparkle"];
    }
	
    return result;
}

BOOL SUSetupSupportFolder()
{
	return [[BLAuthentication sharedInstance] executeCommand:@"/bin/mkdir" withArgs:[NSArray arrayWithObjects:@"-p", SUSupportFolderPath(), nil]];
}